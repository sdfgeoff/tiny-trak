Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-02-27T15:23:09+13:00

====== Designs ======

There are loads of designs out there, waiting for you to make them.

Most of these designs are on thingiverse under Creative Commons licensing. I've
mirrored them on this site, but you may want to check with the original source
in case there have been updates to the design.

If you have a design and want it added to this site, [[https://gitlab.com/sdfgeoff/tiny-trak/issues|drop an issue on this sites repository.]]

===== Lego Tracks =====

==== Tiny Trak - SnappyFPV ====
This was the video that got many people interested in Tiny Traks. The build uses two continous rotation servos and lego tracks.
[[./Tiny_Trak_-_Various_Parts.zip|Zip file of STL's]]. [[https://www.thingiverse.com/thing:2896083|Thingiverse Link]]
<iframe width="560" height="315" src="https://www.youtube.com/embed/cEEd3qQv2lE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



===== Printable Tracks (TPU + PLA) =====


===== Other Tracks =====




