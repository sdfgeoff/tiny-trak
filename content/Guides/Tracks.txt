Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-07-19T19:17:14+02:00

====== Tracks ======

The tracks are the most important part of a Tiny Trak. It's in the name - how could it not be important! Aside from that, it's how the power is conveyed from the motors to the ground, and is what enables your Tiny Track to move. The original Tiny Traks were built using Lego rubber tracks ([[https://brickset.com/parts/4180576/grease-band-151mm-x-2m|part number 4180576]]), These tracks are 18mm wide and 151mm circumference. They have 22 "links" in them. If a vehicle uses lego tracks, they have often been purchased from ebay because lego sets containing them are relatively hard to come by. If you can't find Lego tracks for your vehicle, do not despair. The Tiny Trak community is very DIY, and have come up with [[+Construction|several different ways]] to make your own tracks. One of them will probably fit your budget, tools and materials. 

While most track designs will work, I've put together a bunch of tips about [[+Sizing|designing a successful track system.]]

