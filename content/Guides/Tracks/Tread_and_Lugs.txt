Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-07-20T17:17:00+02:00

====== Tread and Lugs ======

==== Track-wheel interface ====
Your wheels have to grip onto your track somehow. This can either be done by lugs that protrude from the belt and go into indents on the wheels, or it can be done by having indents on the wheels and teeth that protrude from the wheels. 

==== Tread Design ====
