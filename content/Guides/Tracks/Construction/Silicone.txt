Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-02-10T12:15:45+13:00

====== Silicone ======

===== Process Overview =====
1. Design a mold
2. Wrap thread in the mold. Silicone is stretchy so embedding a thread inside the silicone prevents the track being like a rubber band.
3. Fill with silicone. Either you can fill with a popsicle stick or you can design the mold to screw onto a caulking gun.
4. Wait till dry. Silicone cures at about 2mm/day. So you generally have to wait about 24 hours.
5. Remove track from mold. 


===== Silicone Selection =====
Silicone is a material you can buy at your hardware store in the sealants section. It comes in 300ml tubes and is used for sealing bathrooms. There are a couple common types of sealants:

==== Acetoxy Silicone ====
When an acetoxy silicone cures it releases the smell of vinegar. As far as I understand this is because the curing process creates acetic acid. Normally this silicone sets at about 2mm/24 hours and the curing process can be accelerated by adding various compounds. Typically used are cornflour, glycerin or baking powder. I found that normal wheat flour worked fine. The internet tells me that these accelerates work by releasing moisture into the silicone as it cures, and this results in the silicone curing completely (even in the middle) in an hour or two.

This type of silicone does not stick to things very well, so there will be no issues getting it out from a 3D printed PLA mold without a release agent.

Because acetoxy silicone is acidic and doesn't really stick most of the silicones at hardware stores are neutral cure. I've found it advertised as "glazing silicone" and "no mould" silicone. If it's advertised as "odourless" or "non-corrosive" then it's not an acetoxy silicone.

==== Neutral Cure ====
Most hardware store silicones are neutral cure. They release various other chemicals when they cure (Alchohol compounds I think?). There are several properties that make this silicone harder to work with:
1. The accelerants doen't work, so you have to let it cure at 2mm/24hours. If your part is 10mm thick, it can take days to cure.
2. it sticks to things quite well - you need to use a release agent on the mold. To date I haven't found a nice release agent. I tried cooking spray without success.
3. It tends to be less elastic. Of the two neutral cure silicones I tried, one set very hard and the other was soft, but didn't resist tension and you could pull it apart easily

It's worth noting that even a "100% silicone" can be a neutral cure. The different chemical formulation apparently doesn't affect whatever it is that makes things silicone.

==== Acryl/Latex Sealers ====
I know nothing about these. They are sold in the same 300ml cartridges, so I may buy one and give it a go. I think they're even more glue like than neutral cure silicone.

==== Summary ====
If you can, don't waste your time and just use an acetoxy silicone. You can accelerate the curing using common household substances and it comes out of molds easily. The only problem is that it most commonly comes in white clear and transparent. I've only rarely seen it in black.

===== Mold Design =====

==== Caulking gun as silicone injector ====
In my initial experiments using Silicone as a material, I used a popsicle stick to scrape silicone into the mold. This allowed mixing the silicone with accelerants, but was rather messy and when trying to make larger tracks the time to fill the mold becomes significant. After a failed track molding, I wondered if I could use a caulking gun as an injector. Because you can't mix the silicone with an accelerant, this method is only suitable for use with thin parts (2mm/24hours curing time for normal silicone) or a type of silicone that advertises itself as "fast curing"

With a 3D printer, I made a mold that would simply screw on to the end of the tube of silicone. With a couple of squishes of the handle the mold was completely filled! It was quick, easy and painless.
{{../caulking_gun_injector.jpg?width=200}}

Using this method the silicone is forced into the mold and tends to go everywhere inside the mold. If your 3D printer isn't perfectly set up you may find the silicone gets inside the 3D print at sharp corners! It also means that the silicone gets really wedged into the layers, so using a mold release is definitely a good idea.
