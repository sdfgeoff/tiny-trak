python3 ./tools/zim/zim.py --index content/notebook.zim

rm -rf public/

python3 ./tools/zim/zim.py --export \
	--format=html --template=tools/export_template/template.html \
	--output=./public \
	--overwrite \
	--verbose \
  content/notebook.zim

cp -a tools/export_template/ public/_resources
