"use strict"
var children_map = {};
var url_to_name = {}

window.onload = function(current_url){
	var critical_path = [];
	var page_name = ""
	
	let menu_widget = document.getElementById("links")
	
	// List of [element, path_to_element, parent_url]
	var to_search = []
	for (let child of menu_widget.children[0].children){
		to_search.push([child, [], ''])
	}
	
	while (to_search.length > 0){
		var current_widget = to_search.shift()
		var current_item = current_widget[0]
		var current_path = current_widget[1]
		var parent_url = current_widget[2]
		
		if (current_item.tagName != "LI"){
			console.error("ABORT. List contained non list item")
		}
		var element_data = current_item.children[0]
		
		if (element_data.tagName == "A"){
			// Normal child
			var current_name = element_data.title
			var current_url = element_data.href
		} else if (element_data.tagName == "B"){
			// This is the current page!!!
			var current_name = element_data.innerHTML
			var current_url = window.location.href
			page_name = current_name
			critical_path = current_path
			critical_path.push(current_url)
		}
		
		// Insert into data structures
		url_to_name[current_url] = current_name

		if (!(parent_url in children_map)) {
			children_map[parent_url] = new Array()
		}
		children_map[parent_url].push(current_url)
		
		// Recurse
		var additional_elements = current_item.children[1]
		if (additional_elements){
			for (let child of additional_elements.children){
				let child_path = current_path.concat([current_url])
				to_search.push([child, child_path, current_url])
			}
		}
		
	}
	
	let breadcrumbs = document.getElementById("breadcrumbs")
	let page_children_owner = document.getElementById("page_children")
	let index_link = document.getElementById("index_link")

	let last_url = ''
	let last_item = index_link
	// Build breadcrumbs
	for (let url of critical_path){
		var arrow = document.createElement("li")
		arrow.innerHTML = '>'
		breadcrumbs.appendChild(arrow)
		var item = document.createElement("li")
		var link = document.createElement("a")
		link.innerHTML = url_to_name[url]
		link.href = url
		item.appendChild(link)
		breadcrumbs.appendChild(item)
		
		if (children_map[url]){
			last_url = url
			last_item = link
		}
		
		console.log("HERE", url)
	}
	
	last_item.className = "children_tip"
	generate_menu(page_children_owner, last_url)
	
	menu_widget.parentNode.removeChild(menu_widget)
}

function generate_menu(root_element, start_url){
	console.log("Generating menu for ", start_url)
	let list_root = document.createElement("ul")
	list_root.className = "page_children"
	let child_list = children_map[start_url]
	if (child_list){
		child_list.sort()
		for (let child_url of child_list){
			let item = document.createElement("li")
			let link = document.createElement("a")
			link.href = child_url
			link.innerHTML = url_to_name[child_url]
			item.appendChild(link)
			list_root.appendChild(item)
		}
	}
	root_element.appendChild(list_root)
}
